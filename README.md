# Advanced CI/CD Workshop February 22 2022

Workshop materials for Advanced CI/CD Workshop, February 22, 2022

[Workshop User Guide Download](https://gitlab.com/bradleylee/advanced-ci-cd-workshop-feb-22-2022/-/raw/main/assets/doc/22%20Feb%202022%20Advanced%20CI_CD%20Virtual%20Workshop%20v1.pdf?inline=false)

## How To Access the GitLab Training Cloud

[Redemption Code Video Walkthrough](https://gitlab.com/bradleylee/advanced-ci-cd-workshop-feb-22-2022/-/raw/main/assets/doc/How_to_access_the_GitLab_Training_Cloud.mov?inline=false)

Instructions will be provided by the instructor, and can also be accessed via
[video](https://gitlab.com/bradleylee/advanced-ci-cd-workshop-feb-22-2022/-/raw/main/assets/doc/How_to_access_the_GitLab_Training_Cloud.mov?inline=false)
or condensed written instructions below.

### GitLab Demo Login/Registration

1. Access the [GitLab Demo Cloud](https://gitlabdemo.com/login)
1. Redeem the invitation code you received via e-mail
   ![Redeem](./assets/img/redeem.png)
   ![Create](./assets/img/create.png)
1. Make sure to `Download Credentials`

#### Option A

1. On the same page as `Download Credentials` above, click the blue button
   labeled: `My Group`
1. You should be redirected to the group the serves as the namespace for
   creating your projects

#### Option B

1. Upon login, click `☰ Menu` -> `Groups` -> `Your Groups`
1. Click `My Test Group - <userid>` to get to your personal user group page
   ![Login](./assets/img/login.png)

## Create Initial Workshop Project

[Project Import Guide](https://gitlab.com/bradleylee/advanced-ci-cd-workshop-feb-22-2022/-/raw/main/assets/doc/Project%20Import.pdf?inline=false)

1. Navigate to your personal group page using one of the above options
1. Click `New Project` -> `Create From Template`
1. Use the `advanced-ci-cd-workshop` template
1. Set the Project Name to `My Project 1`, leave the other values at default
1. Click `Create Project`
